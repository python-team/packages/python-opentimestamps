python-opentimestamps (0.4.2-2) UNRELEASED; urgency=medium

  * Bump debhelper from old 11 to 13.
  * Update renamed lintian tag names in lintian overrides.
  * Remove obsolete field Name from debian/upstream/metadata (already present in
    machine-readable debian/copyright).
  * Update standards version to 4.6.1, no changes needed.

 -- Debian Janitor <janitor@jelmer.uk>  Sat, 05 Nov 2022 01:50:37 -0000

python-opentimestamps (0.4.2-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Patch: Migrate from pysha3 to pycryptodomex, which is still maintained
    upstream.
  * autopkgtest: Write an openssl config file to enable ripemd160.

 -- Stefano Rivera <stefanor@debian.org>  Sat, 29 Oct 2022 14:56:44 +0200

python-opentimestamps (0.4.1-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Sandro Tosi ]
  * Use the new Debian Python Team contact name and address

 -- Sandro Tosi <morph@debian.org>  Fri, 03 Jun 2022 23:08:03 -0400

python-opentimestamps (0.4.1-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat.
  * d/control: Remove ancient X-Python3-Version field.
  * Bump Standards-Version to 4.4.1.

  [ Hanno Stock ]
  * New upstream release 0.4.1
  * Added autopkgtests

 -- Hanno Stock <opensource@hanno-stock.de>  Tue, 29 Oct 2019 17:33:03 +0100

python-opentimestamps (0.4.0-1) unstable; urgency=medium

  * Initial release (Closes: #921261)

 -- Hanno Stock <opensource@hanno-stock.de>  Tue, 05 Feb 2019 18:24:45 +0000
